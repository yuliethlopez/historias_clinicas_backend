package com.sistema.examenes.repositorios;

import com.sistema.examenes.modelo.Historia_Clinica;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface Historia_ClinicaRepository extends JpaRepository<Historia_Clinica,Long> {

    public List<Historia_Clinica> findByIdentificacion(String identificacion);

}
