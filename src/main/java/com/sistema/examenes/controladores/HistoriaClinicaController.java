package com.sistema.examenes.controladores;


import com.sistema.examenes.modelo.Historia_Clinica;
import com.sistema.examenes.servicios.HistoriaClinicaService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class HistoriaClinicaController {

    @Autowired
    private HistoriaClinicaService historiaClinicaService;


    @GetMapping("/historias")
    public List<Historia_Clinica> index(){
        return historiaClinicaService.findAll();
    }


    @PostMapping("/historias")
    @ResponseStatus(HttpStatus.CREATED)
    public Historia_Clinica guardarHistoria(@RequestBody Historia_Clinica historia_clinica){
        return historiaClinicaService.guardarHistoria(historia_clinica);
    }

    @GetMapping("/historias/{identificacion}")
    public ResponseEntity<?>  buscarHistoriaXIdentificacion(@PathVariable String identificacion){
        List<Historia_Clinica> historiaClinica = historiaClinicaService.obtenerHistoria(identificacion);
        return new ResponseEntity<List<Historia_Clinica>>(historiaClinica, HttpStatus.OK);
    }

    @DeleteMapping("/historias/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarUsuario(@PathVariable("identificacion") Long id){
        historiaClinicaService.delete(id);
    }

}
