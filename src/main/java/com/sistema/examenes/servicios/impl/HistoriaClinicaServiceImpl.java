package com.sistema.examenes.servicios.impl;

import com.sistema.examenes.modelo.Historia_Clinica;
import com.sistema.examenes.repositorios.Historia_ClinicaRepository;
import com.sistema.examenes.servicios.HistoriaClinicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;


@Service
public class HistoriaClinicaServiceImpl implements HistoriaClinicaService {

    @Autowired
    private Historia_ClinicaRepository historia_clinicaRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Historia_Clinica> findAll() {
        return (List<Historia_Clinica>)historia_clinicaRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Historia_Clinica> obtenerHistoria(String identificacion) {
        return historia_clinicaRepository.findByIdentificacion(identificacion);
    }

    @Override
    public Historia_Clinica guardarHistoria(Historia_Clinica historia_clinica) {
        Historia_Clinica historiaClinicaLocal = historia_clinicaRepository.save(historia_clinica);
        return historiaClinicaLocal;
    }

    @Override
    public void delete(Long id) {
        historia_clinicaRepository.deleteById(id);
    }

    @Override
    public Historia_Clinica findById(Long id) {
        return historia_clinicaRepository.findById(id).orElse(null);
    }
}
