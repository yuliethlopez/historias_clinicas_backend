package com.sistema.examenes.servicios;

import com.sistema.examenes.modelo.Historia_Clinica;

import java.util.List;
import java.util.Optional;

public interface HistoriaClinicaService {

    public List<Historia_Clinica> findAll();

    public List<Historia_Clinica> obtenerHistoria(String identificacion);

    public Historia_Clinica guardarHistoria(Historia_Clinica historia_clinica);

    public void delete(Long id);

    public Historia_Clinica findById(Long id);

}
